# StuDocu PoC

`yarn` to install.

## Run:

`yarn start` to run on localhost:3000

## Notes

- Local persistance implemented using a LocalStorage API with `redux-saga`, instead of `redux-persist`, for a more complex and real life challenge.
- No error states. We asume the API always returns something valid.
- Applied basic tooltip using CSS's before and after. No JS involved.
- Used `reduxsauce` for easily creating actions and reducers.
- Using `react-bootstrap` for html markup.
- Used `create-react-app` for scaffolding and webpack default config.
- There're no validations (empty Q&A) or confirmations (when deleting all).

The whole task took about 5 hours to complete.