import { createActions } from 'reduxsauce';

/* ----- Types and Action Creators ----- */

const { Types, Creators } = createActions({
  // Fetching
  fetch: null,
  set: ['questions'],
  fetching: ['fetching'],
  // Saving
  save: ['question', 'answer'],
  push: ['question'],
  saving: ['saving'],
  // Sorting
  sort: null,
  // Removing
  removeAll: null,
  clear: null,
  removing: ['removing'],
}, { prefix: 'QUESTION_' });

export const QuestionTypes = Types;
export default Creators;
