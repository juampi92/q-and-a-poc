import { call, put } from 'redux-saga/effects';

import QuestionsActions from '../Actions/questions.actions';

export function * fetchQuestions(api) {
  // make the call to the api
  const response = yield call(api.getQuestions);

  if (response.ok) {
    const questions = response.data;
    yield put(QuestionsActions.set(questions))
  } else {
    yield put(QuestionsActions.set([]))
  }
}

export function * saveQuestion(api, { question, answer }) {
  // make the call to the api
  const response = yield call(api.saveQuestion, { question, answer });

  if (response.ok) {
    const question = response.data;
    yield put(QuestionsActions.push(question))
  }
}

export function * removeAllQuestions(api) {
  // make the call to the api
  yield call(api.removeAllQuestions);

  yield put(QuestionsActions.clear())
}
