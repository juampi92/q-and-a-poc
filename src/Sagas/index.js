import { takeLatest, all } from 'redux-saga/effects';
import API from '../Services/Api';

/* ------------- Types ------------- */

import { QuestionTypes } from '../Actions/questions.actions';

/* ------------- Sagas ------------- */

import { fetchQuestions, saveQuestion, removeAllQuestions } from './QuestionsSagas'

/* ------------- API ------------- */
const api = API();

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
// export const api = API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  try {
    yield all([
      // Questions
      takeLatest(QuestionTypes.FETCH, fetchQuestions, api),
      takeLatest(QuestionTypes.SAVE, saveQuestion, api),
      takeLatest(QuestionTypes.REMOVE_ALL, removeAllQuestions, api),
    ]);
  } catch (err) {
    console.error(err);
  }
};
