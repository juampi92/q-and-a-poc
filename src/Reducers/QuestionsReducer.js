import { createReducer } from 'reduxsauce'
import Immutable from 'immutable';

import { QuestionTypes } from '../Actions/questions.actions';

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable.fromJS({
  fetching: false,
  saving: false,
  removing: false,
  list: Immutable.List()
});

/* ------------- Reducers ------------- */

const fetching = (state) =>
  state.set('fetching', true);

const setQuestions = (state, { questions }) =>
  state
    .set('list', Immutable.fromJS(questions))
    .set('fetching', false);

const saving = (state) =>
  state.set('saving', true);

const pushQuestion = (state, { question }) =>
  state
    .set('list', state.get('list').push(Immutable.fromJS(question)))
    .set('saving', false);

const sortQuestions = (state) =>
  state.set('list', state.get('list').sortBy((q) => q.toJS().question));

const removing = (state) =>
  state.set('removing', true);

const clearQuestions = (state) =>
  state
    .set('list', Immutable.fromJS([]))
    .set('removing', false);

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [QuestionTypes.FETCH]: fetching,
  [QuestionTypes.SET]: setQuestions,
  [QuestionTypes.SAVE]: saving,
  [QuestionTypes.PUSH]: pushQuestion,
  [QuestionTypes.SORT]: sortQuestions,
  [QuestionTypes.REMOVE_ALL]: removing,
  [QuestionTypes.CLEAR]: clearQuestions
})