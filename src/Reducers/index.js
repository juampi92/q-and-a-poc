import { combineReducers } from 'redux'
import configureStore from './store'
import rootSaga from '../Sagas/'

export default () => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers({
    questions: require('./QuestionsReducer').reducer,
  })

  return configureStore(rootReducer, rootSaga)
}
