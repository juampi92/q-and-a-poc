import { createStore, applyMiddleware, compose } from 'redux'

import { createLogger } from 'redux-logger'
import createSagaMiddleware from 'redux-saga'

import Immutable from 'immutable'

// creates the store
export default (rootReducer, rootSaga) => {
  /* ------------- Redux Configuration ------------- */

  const middleware = []
  const enhancers = []

  /* ------------- Saga Middleware ------------- */

  const sagaMiddleware = createSagaMiddleware()
  middleware.push(sagaMiddleware)

  /* ------------- Logger Middleware ------------- */

  // Create the logger
  const logger = createLogger({
    stateTransformer: (state) => {
      let newState = {};
    
      for (var i of Object.keys(state)) {
        if (Immutable.Iterable.isIterable(state[i])) {
          newState[i] = state[i].toJS();
        } else {
          newState[i] = state[i];
        }
      };
    
      return newState; 
    }
  })
  middleware.push(logger)

  /* ------------- Assemble Middleware ------------- */

  enhancers.push(applyMiddleware(...middleware))

  const store = createStore(rootReducer, compose(...enhancers))

  // Kick off root saga
  sagaMiddleware.run(rootSaga)

  return store
}
