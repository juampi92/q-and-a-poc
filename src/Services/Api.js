import uuid from 'uuid/v4'

export default () => {

  const get = () => {
    let data;
    try {
      data = localStorage.getItem('questions');
      data = JSON.parse(data) || [{ id: uuid(), question: 'How to add a question?', answer: 'Easy, just fill the form below and submit it' }];
    } catch (err) {
      data = [];
    }
    return data;
  }

  const set = (arr) => {
    localStorage.setItem('questions', JSON.stringify(arr));
  }

  const getQuestions = () => {
    return new Promise((resolve) => {
      const data = get();
      
      setTimeout(() => resolve({ ok: true, data }), 2000);
    });
  }

  const saveQuestion = (question) => {
    return new Promise((resolve) => {
      let data = get();
      const savedQuestion = { id: uuid(), ...question };
      data.push(savedQuestion);
      set(data);
      
      setTimeout(() => resolve({ ok: true, data: savedQuestion }), 2000);
    });
  }

  const removeAllQuestions = (question) => {
    return new Promise((resolve) => {
      localStorage.setItem('questions', '[]');
      
      setTimeout(resolve, 2000);
    });
    
  }

  return {
    getQuestions,
    saveQuestion,
    removeAllQuestions
  };
};
