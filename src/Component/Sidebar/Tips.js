import React from 'react';

import { connect } from 'react-redux';

const Tips = ({ total }) => {
  return (<div>Here you can find {total} questions. Feel free to create your own questions.</div>);
};

const mapStateToProps = (state, ownProps = {}) => {
  const loading = state.questions.get('fetching');
  return {
    total: loading ? 'some' : state.questions.get('list').toJS().length,
  };
};

export default connect(mapStateToProps)(Tips);
