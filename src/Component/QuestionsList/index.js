import React, { Component } from 'react';

import { Alert, ListGroup, Button } from 'react-bootstrap';

import { connect } from 'react-redux';
import QuestionActions from '../../Actions/questions.actions';

import QuestionItem from './QuestionItem';
import './styles.css';

class QuestionsList extends Component {
  componentDidMount () {
    this.props.fetch();
  }

  renderContent () {
    const { questions } = this.props;

    return (!questions.length) ? this.renderEmpty() : this.renderQuestions(questions);
  }

  renderEmpty () {
    return (<Alert bsStyle='danger'>No questions yet :(</Alert>);
  }

  renderQuestions(questions) {
    const { sort, removeAll, removing } = this.props;

    return (
      <div>
        <ListGroup>
          { questions.map((q) => <QuestionItem key={q.id} question={q} />) }
        </ListGroup>
        <div>
          <Button className='QuestionList__button' bsStyle='primary' onClick={sort}>Sort Questions</Button>
          <Button className='QuestionList__button' bsStyle='danger' onClick={removeAll} disabled={removing}>
            { !removing ? 'Remove Questions' : 'Removing...' }
          </Button>
        </div>
      </div>
    );
  }

  render () {
    const { fetching } = this.props;

    return (
      <section>
        <h3 tooltip='Here you can find the created questions and their answers'>Created questions</h3>
        { fetching ? <div>Fetching questions...</div> : this.renderContent() }
      </section>
    );
  }
}

const mapStateToProps = (state, ownProps = {}) => {
  return {
    fetching: state.questions.get('fetching'),
    removing: state.questions.get('removing'),
    questions: state.questions.get('list').toJS(),
  };
};

const mapDispatchToProps = dispatch => ({
  fetch: () => { dispatch(QuestionActions.fetch()) },
  sort: () => { dispatch(QuestionActions.sort()) },
  removeAll: () => { dispatch(QuestionActions.removeAll()) },
});

export default connect(mapStateToProps, mapDispatchToProps)(QuestionsList);
