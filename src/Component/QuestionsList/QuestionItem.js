import React, { Component } from 'react';

import { ListGroupItem } from 'react-bootstrap';

export default class QuestionItem extends Component {
  constructor (props, context) {
    super(props, context);

    this.state = {
      hidden: true
    }
     
    this.handleShow = this.handleShow.bind(this)
  }

  handleShow () {
    this.setState({
      hidden: false
    });
  }

  render () {
    const { question } = this.props
    const { hidden } = this.state

    return (
      <ListGroupItem onClick={this.handleShow}>
        <div className='QuestionItem__question'>{ question.question }</div>
        { hidden ? null : question.answer }
      </ListGroupItem>
    );
  }
}