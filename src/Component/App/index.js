import React, { Component } from 'react';

import Header from '../Header';
import Main from '../Main';
import SidebarTips from '../Sidebar/Tips';

import { Row, Col } from 'react-bootstrap';
import './style.css';
import './tooltips.css';

class App extends Component {
  render() {
    return (
      <div className='App'>
        <Header />
        <div className='container'>
          <Row>
            <Col xs={12} md={3} className='App__sidebar'>
              <SidebarTips />
            </Col>
            <Col xs={12} md={9} className='App__content'>
              <Main />
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default App;
