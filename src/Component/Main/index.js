import React from 'react';

import QuestionsList from '../QuestionsList';
import QuestionsCreate from '../QuestionsCreate';

const Main = () => (
  <main className="main-layout">
    <QuestionsList />
    <QuestionsCreate />
  </main>
);

export default Main;
