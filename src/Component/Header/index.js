import React, { Component } from 'react';

import './style.css';

export default class Header extends Component {
  render () {
    return (<div className='container Header'>
      <h1 className='Header__title'>The Awesome Q/A Tool</h1>
    </div>);
  }
}