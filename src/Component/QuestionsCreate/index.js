import React, { Component } from 'react';

import { connect } from 'react-redux';
import QuestionActions from '../../Actions/questions.actions';

import { FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';

class QuestionsCreate extends Component {
  constructor (props, context) {
    super(props, context);

    this.state = {
      question: '',
      answer: ''
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  componentDidUpdate (pastProps) {
    // If it was saving but now it's not, restart state
    if (pastProps.saving && !this.props.saving) {
      // Restart state
      this.setState({
        question: '',
        answer: ''
      })
    }
  }

  handleInputChange (event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit (e) {
    e.preventDefault();

    const { question, answer } = this.state;

    this.props.store({ question, answer });
  }

  render () {
    const { question, answer } = this.state;
    const { saving } = this.props;

    return (
      <div>
        <h3 tooltip='Here you can create new questions and their answers'>Create a new Question</h3>
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId='formBasicText'>
            <ControlLabel>Question</ControlLabel>
            <FormControl
              type='text'
              name='question'
              value={question}
              disabled={saving}
              placeholder='Enter your question'
              onChange={this.handleInputChange}
            />
            <FormControl.Feedback />
          </FormGroup>
          <FormGroup controlId='formBasicText'>
            <ControlLabel>Answer</ControlLabel>
            <FormControl
              componentClass='textarea'
              name='answer'
              value={answer}
              disabled={saving}
              onChange={this.handleInputChange}
            />
            <FormControl.Feedback />
          </FormGroup>
          <Button type='submit' bsStyle='success' disabled={saving}>{ !saving ? 'Create question' : 'Creating...'}</Button>
        </form>
      </div>
    );
  }
};

const mapStateToProps = (state, ownProps = {}) => {
  return {
    saving: state.questions.get('saving'),
  };
};

const mapDispatchToProps = dispatch => ({
  store: ({ question, answer }) => { dispatch(QuestionActions.save(question, answer)) }
});

export default connect(mapStateToProps, mapDispatchToProps)(QuestionsCreate);
